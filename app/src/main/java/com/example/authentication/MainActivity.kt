package com.example.authentication

import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        auth = Firebase.auth
        signUpButton.setOnClickListener {
            signUp()

        }
    }
    private fun signUp(){
        val email = emailEditText.text.toString()
        val password = passwordlEditText.text.toString()
        val repeatpassword = repeatPasswordlEditText.text.toString()

        if(email.isNotEmpty() && password.isNotEmpty() && repeatpassword.isNotEmpty()){
            if(password == repeatpassword){
                signUpButton.isClickable = false
                ProgressBar.visibility = View.VISIBLE
                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        ProgressBar.visibility = View.GONE
                        signUpButton.isClickable = true
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            d("signUp", "createUserWithEmail:success")
                            val user = auth.currentUser

                        } else {
                            // If sign in fails, display a message to the user.
                            d("signUp", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()

                        }

                    }


            }else{
                Toast.makeText(this, "Please enter same passwords", Toast.LENGTH_SHORT).show()
            }

        }else{
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
        }
    }

}